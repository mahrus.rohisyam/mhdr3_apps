import React from 'react';
import Navigation from './src/navigation/index';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {Persistor, Store} from './src/redux/store';

const App = () => {
  return (
    <Provider store={Store}>
      <PersistGate persistor={Persistor}>
        <Navigation />
      </PersistGate>
    </Provider>
  );
};

export default App;
