import React, {Component} from 'react';
import {Text, StyleSheet, View, SafeAreaView, StatusBar} from 'react-native';

export default class Layout extends Component {
  render() {
      const {style} = this.props
    return (
      <SafeAreaView style={{flex:1}}>
        {/* <StatusBar barStyle='light-content' /> */}
        <View style={{...styles.mainContainer, ...style}}>{this.props.children}</View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    mainContainer:{
        flex:1
    }
});
