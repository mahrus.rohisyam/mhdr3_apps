import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class Card extends Component {
    render() {
        return (
            <View style={styles.mainContainer}>
                <Text> textInComponent </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer:{
        padding:10,
        borderRadius:20,
    }
})
