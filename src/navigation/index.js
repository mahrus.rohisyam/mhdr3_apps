import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { EAxios, CRUD_Statement, Film, Home, Lifecycle, Login, PropsCom, Splash, Statement, Playground, Redux, Fungame, Firebase, Signup } from '../screens';

const Stack = createNativeStackNavigator();
const hide = {headerShown: false}

function index() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Splash'>
        <Stack.Screen options={hide} name="Home" component={Home} />
        <Stack.Screen options={hide} name="Firebase" component={Firebase} />
        <Stack.Screen options={hide} name="Splash" component={Splash} />
        <Stack.Screen options={hide} name="Lifecycle" component={Lifecycle} />
        <Stack.Screen options={hide} name="Propscom" component={PropsCom} />
        <Stack.Screen options={hide} name="Film" component={Film} />
        <Stack.Screen options={hide} name="Statement" component={Statement} />
        <Stack.Screen options={hide} name="Login" component={Login} />
        <Stack.Screen options={hide} name="CRUD_Statement" component={CRUD_Statement} />
        <Stack.Screen options={hide} name="EAxios" component={EAxios} />
        <Stack.Screen options={hide} name="Playground" component={Playground} />
        <Stack.Screen options={hide} name="Redux" component={Redux} />
        <Stack.Screen options={hide} name="Fungame" component={Fungame} />
        <Stack.Screen options={hide} name="Signup" component={Signup} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default index;