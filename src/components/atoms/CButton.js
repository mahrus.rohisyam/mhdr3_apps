import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import { CText } from '.';
import {colors} from '../../assets';

export default class CButton extends Component {
  render() {
    const {style, textStyle} = this.props;
    
    return (
      <View>
        <TouchableOpacity {...this.props} style={{...styles.button, ...style}}>
          <CText style={{...{fontWeight: 'bold', color: 'white'}, ...textStyle}}>
            {this.props.title.toUpperCase()}
          </CText>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    padding: 10,
    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    elevation: 5,
  },
});
