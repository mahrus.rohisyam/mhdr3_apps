import React, {Component} from 'react';
import {StyleSheet, View, StatusBar, ScrollView, SafeAreaView} from 'react-native';

export default class Layout extends Component {
  render() {
    const {style, scroll} = this.props;

    return (
      <>
        <SafeAreaView style={{...style, ...styles.mainContainer}}>
        <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
          {scroll ? (
            <ScrollView style={{flex:1}}>
              {this.props.children}
            </ScrollView>
          ) : (
            <View>{this.props.children}</View>
          )}
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
});
