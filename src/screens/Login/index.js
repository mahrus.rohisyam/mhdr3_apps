import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Button,
  ToastAndroid,
} from 'react-native';
import {CTextInput, Layout} from '../../components';
import MCI from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../../assets';
import axios from 'axios';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import {connect} from 'react-redux';

export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secure: true,
      data: {},
      email: '',
      password: '',
      currentUser: [],
    };
  }

  componentDidMount() {
    axios
      .get('https://dummyjson.com/users', {
        params: {
          limit: 50,
        },
      })
      .then(res => this.setState({data: res.data.users}))
      .catch(e => ToastAndroid.show(`Error ${e}`, ToastAndroid.SHORT));
    // console.log(dataType)
  }

  _viewPassword() {
    const {secure} = this.state;
    this.setState({secure: !secure});
  }

  _submit = () => {
    const {data, email, password} = this.state;
    const {navigation, login, currentUser} = this.props;
    // const filter = data.filter(value => {
    //   return value.email == email && value.password == password;
    // });
    // filter.length > 0
    //   ? ToastAndroid.show('Success :)', ToastAndroid.SHORT)
    //   : console.log('Failed :(');

    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        login(auth().currentUser);
        navigation.replace('Home')
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!');
        }

        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }

        console.error(error);
      });

      console.log('Pressed')
  };

  render() {
    const {secure, data, email, password} = this.state;
    const {navigation} = this.props;

    return (
      <Layout>
        <View style={[styles.wrapper]}>
          <View>
            <Text style={styles.title}>Welcome Back Comrades !</Text>
            <Text style={styles.subtitle}>Enter your main and roll on.</Text>
          </View>
        </View>

        <View style={styles.wrapper}>
          <View>
            <Text>Email</Text>
            <CTextInput
              onChangeText={type => {
                this.setState({email: type});
              }}
              placeholder="Email"
            />
          </View>
          <View style={{marginTop: 20}}>
            <Text>Password</Text>
            <CTextInput
              onChangeText={type => {
                this.setState({password: type});
              }}
              secureTextEntry={secure}
              placeholder="Password"
              firstIcon="lock-check"
              secure
              secondIcon={secure ? 'eye' : 'eye-off'}
              onPress={() => {
                this._viewPassword();
              }}
            />
          </View>
        </View>

        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Signup');
          }}
          activeOpacity={0.5}
          style={{marginHorizontal: 25, justifyContent: 'center'}}>
          <Text style={{color: colors.primary}}>Create account</Text>
        </TouchableOpacity>

        <View style={[styles.wrapper]}>
          {email.length > 0 && password.length > 0 && (
            <Button
              color={colors.primary}
              title="Submit"
              onPress={() => {
                this._submit();
              }}
            />
          )}
        </View>
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: data => {
      dispatch({
        type: 'LOGIN',
        payload: data,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(index);

const styles = StyleSheet.create({
  btn: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    borderWidth: 1,
  },
  wrapper: {
    margin: 25,
  },
  title: {
    marginTop: 30,
    fontSize: 20,
    fontWeight: 'bold',
  },
});
