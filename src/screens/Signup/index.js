import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Button,
  ToastAndroid,
} from 'react-native';
import {CTextInput, Gap, Layout} from '../../components';
import MCI from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../../assets';
import axios from 'axios';
import auth from '@react-native-firebase/auth';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secure: true,
      data: {},
      email: '',
      password: '',
      currentUser: [],
    };
  }

  componentDidMount() {
    axios
      .get('https://dummyjson.com/users', {
        params: {
          limit: 50,
        },
      })
      .then(res => this.setState({data: res.data.users}))
      .catch(e => ToastAndroid.show(`Error ${e}`, ToastAndroid.SHORT));
    // console.log(dataType)
  }

  _viewPassword() {
    const {secure} = this.state;
    this.setState({secure: !secure});
  }

  _submit = () => {
    const {email, password} = this.state;
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        console.log('User account created & signed in!');
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!');
        }
        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }
        console.error(error);
      });
  };

  render() {
    const {secure, data, email, password, username} = this.state;
    const {navigation} = this.props;

    return (
      <Layout>
        <View style={[styles.wrapper]}>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => navigation.goBack()}>
            <MCI name="arrow-back" size={35} />
          </TouchableOpacity>
          <View>
            <Text style={styles.title}>Lets Sign Up !</Text>
            <Text style={styles.subtitle}>
              Sign up and feel the atmosphere.
            </Text>
          </View>
        </View>

        <View style={styles.wrapper}>
          {/* <View>
            <Text>Username</Text>
            <CTextInput
              onChangeText={type => {
                this.setState({username: type});
              }}
              placeholder="Username"
            />
          </View> */}
          {/* <Gap height={25} /> */}
          <View>
            <Text>Email</Text>
            <CTextInput
              onChangeText={type => {
                this.setState({email: type});
              }}
              placeholder="Email"
            />
          </View>

          <View style={{marginTop: 20}}>
            <Text>Password</Text>
            <CTextInput
              onChangeText={type => {
                this.setState({password: type});
              }}
              secureTextEntry={secure}
              placeholder="Password"
              firstIcon="lock-check"
              secure
              secondIcon={secure ? 'eye' : 'eye-off'}
              onPress={() => {
                this._viewPassword();
              }}
            />
          </View>
        </View>

        <View style={[styles.wrapper]}>
          {email.length > 5 && password.length > 5 && (
            <Button
              color={colors.primary}
              title="Sign up"
              onPress={() => {
                this._submit();
              }}
            />
          )}
        </View>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  btn: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    borderWidth: 1,
  },
  wrapper: {
    margin: 25,
  },
  title: {
    marginTop: 30,
    fontSize: 20,
    fontWeight: 'bold',
  },
});
