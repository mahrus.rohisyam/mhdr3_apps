import React, {Component} from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import {Wallpaper} from '../../assets';

export class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [
        {name: 'Mahrus', address: 'Bekasi'},
        {name: 'Farid', address: 'Lampung'},
        {name: 'Subhan', address: 'Jakarta'},
        {name: 'Pur', address: 'Purwokerto'},
        {name: 'Joni', address: 'Tangerang'},
        {name: 'Dimitri', address: 'Rusia'},
      ],
    };
    console.log('Ini dari Constructor');
  }

  componentDidMount() {
    console.log('Didmount');
    const {data} = this.state;
    // const newData = data.filter(value => {
    //   return value.name == 'Mahrus';
    // });

    // this.setState({
    //   data: newData,
    // });

  }

  componentWillUnmount() {
    console.log('Ini dari Wil Unmount');
    
  }

  componentDidUpdate() {
    console.log('Ini dari didUpdate');
  }

  render() {
    const {data} = this.state;

    console.log('Ini dari Render');
    return (
      <View>
        <ScrollView>
          {data.map((value, i) => {
            return (
              <View
                key={i}
                style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                <Text>{value.name}</Text>
                <Text>{value.address}</Text>
              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

export default index;
