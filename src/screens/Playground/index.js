import React, {Component} from 'react';
import {Text, View} from 'react-native';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      data: 'Test',
      buttonList: [
        {name: 'Fibonnacci', action: 'None'},
        {name: 'Fibonnacci', action: 'None'},
        {name: 'Fibonnacci', action: 'None'},
        {name: 'Fibonnacci', action: 'None'},
        {name: 'Fibonnacci', action: 'None'},
      ],
      array: [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        20,
      ],
    };
  }

  render() {
    const {data, array} = this.state;

    return (
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        <Text style={{fontFamily: 'Poppins-Bold'}}>{data}</Text>
        {array.map((value, i) => {
          return <View>{value % 2 != 0 && <Text>{value}</Text>}</View>;
        })}
      </View>
    );
  }
}
