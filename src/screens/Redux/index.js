import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  Button,
  TouchableOpacity,
} from 'react-native';
import {CButton, Gap, Layout} from '../../components';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import {colors, unique_id} from '../../assets';
import {connect} from 'react-redux';

export class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      name: '',
      address: '',
      // id: '',
      onSelect: false,
      onEdit: false,
      checked: true,
    };
  }

  componentDidMount() {}

  _addData = () => {
    const {name, address} = this.state;
    const id = unique_id();
    const data = {name, address, id};
    this.props.add(data);
    this.setState({
      name: '',
      address: '',
      id: '',
    });
  };

  _onEdit = value => {
    this.setState({
      onEdit: !this.state.onEdit,
      id: value.id,
      name: value.name,
      address: value.address,
    });
  };

  _delete = id => {
    this.props.delete(id);
  };

  _submitUpdate = () => {
    const {id, name, address} = this.state;
    const {updateData, students} = this.props;
    const data = {id, name, address};
    const updateStudents = students.map(value => {
      if (value.id === data.id) {
        (value.id = data.id),
          (value.name = data.name),
          (value.address = data.address);
        return value;
      } else {
        return value;
      }
    });
    console.log(updateStudents)
    updateData(updateStudents);
    this.setState({
      onEdit: false,
      name: '',
      address: '',
    });
  };

  _cancel = () => {
    this.setState({
      name: '',
      id: '',
      address: '',
      onEdit: false,
    });
  };

  render() {
    const {data, name, address, id, onEdit, checked} = this.state;
    const {students} = this.props;
    const white = 'white';

    return (
      <Layout>
        <Text style={{textAlign: 'center', marginVertical: 10}}>
          Crud in Redux
        </Text>
        <View style={styles.card}>
          <View style={styles.tableHeader}>
            <Text style={styles.number}>ID</Text>
            <Text style={styles.title}>Name</Text>
            <Text style={styles.title}>Address</Text>
            <Text style={{width: '20%', textAlign: 'center'}}>Action</Text>
          </View>
          {students &&
            students.map((v, i) => {
              return (
                <View key={i} style={styles.tableHeader}>
                  <Text style={styles.number}>{i + 1}</Text>
                  <Text style={styles.title}>{v.name}</Text>
                  <Text style={styles.title}>{v.address}</Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      width: '20%',
                      justifyContent: 'center',
                    }}>
                    <TouchableOpacity>
                      <MCI
                        name="trash-can"
                        onPress={() => this._delete(v.id)}
                        size={30}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        this._onEdit(v);
                      }}>
                      <MCI name="file-edit-outline" size={30} />
                    </TouchableOpacity>
                  </View>
                </View>
              );
            })}
        </View>

        <View style={styles.textInputWrapper}>
          {/* <TextInput
            keyboardType="numeric"
            placeholderTextColor={white}
            value={id}
            onChangeText={type => {
              this.setState({id: type});
            }}
            style={{width: '100%', color: colors.primary}}
            placeholder="Id"
          /> */}
          <TextInput
            placeholderTextColor={white}
            value={name}
            onChangeText={type => {
              this.setState({name: type});
            }}
            style={{width: '100%', color: colors.primary}}
            placeholder="Name"
          />
          <TextInput
            placeholderTextColor={white}
            value={address}
            onChangeText={type => {
              this.setState({address: type});
            }}
            style={{width: '100%', color: colors.primary}}
            placeholder="Address"
          />
          {onEdit ? (
            <>
              <CButton title="update data" onPress={this._submitUpdate} />
              <Gap height={10} />
              <CButton
                title="cancel"
                style={{backgroundColor: 'red'}}
                onPress={this._cancel}
              />
            </>
          ) : name.length > 0 && address.length > 0 ? (
            <CButton title="ADD DATA" onPress={this._addData} />
          ) : (
            <View style={styles.fakeButton}>
              <Text>INSERT DATA</Text>
            </View>
          )}
        </View>
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return {
    students: state.students,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    add: data => {
      dispatch({
        type: 'ADD-STUDENT',
        payload: data,
      });
    },

    delete: data => {
      dispatch({
        type: 'DELETE-STUDENT',
        payload: data,
      });
    },

    updateData: v => {
      dispatch({
        type: 'UPDATE-DATA',
        payload: v,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(index);

const styles = StyleSheet.create({
  card: {
    padding: 10,
    borderRadius: 20,
    borderWidth: 1,
    margin: 5,
  },
  textInput: {
    justifyContent: 'space-around',
    marginTop: 15,
    flexDirection: 'row',
  },
  tableHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  spacer: {
    marginHorizontal: 10,
  },
  number: {
    width: '10%',
    alignItems: 'center',
  },
  title: {
    width: '30%',
  },
  textInputWrapper: {
    margin: 5,
    padding: 10,
    backgroundColor: '#1e1e1e',
    borderRadius: 20,
  },
  fakeButton: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'grey',
    borderRadius: 5,
  },
});
