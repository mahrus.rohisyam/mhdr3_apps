import React, {Component} from 'react';
import {Text, StyleSheet, View, Button, TextInput} from 'react-native';
import {CButton, CText, Layout} from '../../components';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import auth from '@react-native-firebase/auth';
import {connect} from 'react-redux';
export class index extends Component {
  constructor() {
    super();
    this.state = {
      menuList: [
        {name: 'Lifecycle', route: 'Lifecycle'},
        {name: 'Props Communication', route: 'Propscom'},
        {name: 'Film API', route: 'Film'},
        {name: 'Statement', route: 'Statement'},
        {name: 'Login', route: 'Login'},
        {name: 'CRUD Statement', route: 'CRUD_Statement'},
        {name: 'Axios', route: 'EAxios'},
        {name: 'Playground', route: 'Playground'},
        {name: 'Redux', route: 'Redux'},
        {name: 'Fun Game', route: 'Fungame'},
        {name: 'Firebase', route: 'Firebase'},
      ],
    };
  }

  _signOut = () => {
    const {deleteUser, currentUser, navigation} = this.props;
    auth()
    .signOut()
    .then(() => {
      deleteUser(null)
      console.log(currentUser)
      navigation.replace('Splash')
    });
  };

  render() {
    const {data, menuList, test} = this.state;
    const {navigation, currentUser} = this.props;

    return (
      <Layout style={{justifyContent: 'center', alignItems: 'center'}}>
        <Text>Welcome To Class !</Text>
        <Text>Mau belajar apa harini ?</Text>
        {menuList.map((v, i) => {
          return (
            <View style={{marginVertical: 5}} color key={i}>
              <Button
                color={i % 2 == 0 ? '#ffae00' : 'blue'}
                title={v.name}
                onPress={() => {
                  navigation.navigate(v.route);
                }}
              />
            </View>
          );
        })}
        <Button
          title="Signout"
          color="red"
          onPress={() => {
            this._signOut();
          }}
        />
      </Layout>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    deleteUser: data => {
      dispatch({
        type: 'LOGIN',
        payload: data,
      });
    },
  };
};

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(index);

const styles = StyleSheet.create({
  card: {
    margin: 10,
    padding: 5,
    borderWidth: 0.5,
    borderRadius: 10,
    elevation: 5,
    backgroundColor: 'white',
  },
});
