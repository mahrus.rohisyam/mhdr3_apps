import React, {Component} from 'react';
import {Text, StyleSheet, View, Image} from 'react-native';
import {connect} from 'react-redux';
import {Salt} from '../../assets';
import {Layout} from '../../components';

export class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    const {navigation, currentUser} = this.props;
    currentUser != null
      ? setTimeout(() => {
          navigation.replace('Home');
        }, 2000)
      : setTimeout(() => {
          navigation.replace('Login');
        }, 2000);
    console.log(currentUser);
  }

  render() {
    const {data} = this.state;
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <Image source={Salt} resizeMode="contain" style={styles.image} />
        <Text>Welcome To Salt Academy !</Text>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser,
  };
};

export default connect(mapStateToProps)(index);

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'white',
  },
  image: {
    width: '50%',
    height: '20%',
  },
});
