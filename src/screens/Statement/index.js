import React, {Component} from 'react';
import {Text, StyleSheet, View, TextInput, Button} from 'react-native';
import Feather from 'react-native-vector-icons/Feather'

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      address: '',
      id: '',
      data: {
        name: 'Mahrus',
        address: 'Bekasi',
      },
      table: [
        {id: 1, name: 'Mahrus', address: 'Bekasi'},
        {id: 2, name: 'Jono', address: 'Bekasi'},
        {id: 3, name: 'Dono', address: 'Bekasi'},
        {id: 4, name: 'Bejo', address: 'Bekasi'},
      ],
    };
  }

  componentDidMount() {
    const {newId, table} = this.state;
  }

  _addData() {
    const { name, address, table} = this.state;
    const id = table.length + 1
    this.setState({
      table: [...table, {id, name, address}],
      name: '',
      address: '',
      id: '',
    });
    // console.log(table);
  }

  _submit() {
    const {name, address} = this.state;
    this.setState({
      data: {
        name: name,
        address: address,
      },
    });
  }

  render() {
    const {data, name, address, table, id,} = this.state;
    // console.log(table.length, table)
    return (
      <View style={{flex: 1}}>
        <Text style={{textAlign: 'center', marginVertical: 10}}>Statement</Text>
        <View style={styles.card}>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <Text style={{fontWeight: 'bold'}}>Nama:</Text>
            <Text style={{fontWeight: 'bold'}}>Alamat:</Text>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <Text>{data.name}</Text>
            <Text>{data.address}</Text>
          </View>

          <View style={styles.textInput}>
            <TextInput
              onChangeText={input => {
                this.setState({name: input});
              }}
              placeholder="Enter Name"
            />
            <TextInput
              onChangeText={input => {
                this.setState({address: input});
              }}
              placeholder="Enter Address"
            />
          </View>
          <Button
            title="Change State"
            onPress={() => {
              this._submit();
            }}
          />
        </View>

        <View style={styles.card}>
          <View style={styles.tableHeader}>
            <Text style={styles.number}>ID</Text>
            <Text style={styles.title}>Name</Text>
            <Text style={styles.title}>Address</Text>
          </View>
          {table.map((v, i) => {
            return (
              <View key={i} style={styles.tableHeader}>
                <Text style={styles.number}>{v.id}</Text>
                <Text style={styles.title}>{v.name}</Text>
                <Text style={styles.title}>{v.address}</Text>
              </View>
            );
          })}

          <View style={styles.textInputWrapper}>
            <TextInput
              value={name}
              onChangeText={type => {
                this.setState({name: type});
              }}
              style={{width: '100%'}}
              placeholder="Name"
            />
            <TextInput
              value={address}
              onChangeText={type => {
                this.setState({address: type});
              }}
              style={{width: '100%'}}
              placeholder="Address"
            />
            <Button
              title="Change State"
              onPress={() => {
                this._addData();
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    padding: 10,
    borderRadius: 20,
    borderWidth: 1,
    margin: 5,
  },
  textInput: {
    justifyContent: 'space-around',
    marginTop: 15,
    flexDirection: 'row',
  },
  tableHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  spacer: {
    marginHorizontal: 10,
  },
  number: {
    width: '10%',
    alignItems: 'center',
  },
  title: {
    width: '30%',
  },
  textInputWrapper: {
    marginVertical: 5,
    padding: 10,
    backgroundColor: 'orange',
    borderRadius: 20,
  },
});
