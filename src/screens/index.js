import Home from './Home';
import Lifecycle from './Lifecycle';
import Film from './Film';
import PropsCom from './PropsCom';
import Splash from './Splash';
import Statement from './Statement';
import Login from './Login';
import CRUD_Statement from './CRUD_Statement';
import EAxios from './Axios';
import Playground from './Playground';
import Redux from './Redux';
import Fungame from './Fungame'
import Firebase from './Firebase'
import Signup from './Signup'

export {
  Signup,
  Firebase,
  Playground,
  Fungame,
  Home,
  Lifecycle,
  Film,
  PropsCom,
  Splash,
  Statement,
  Login,
  CRUD_Statement,
  EAxios,
  Redux,
};
