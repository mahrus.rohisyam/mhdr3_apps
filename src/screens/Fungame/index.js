import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Button,
  Text,
  TextInput,
  ToastAndroid,
} from 'react-native';
import {CButton, CTextInput} from '../../components';

export default class MyProject extends Component {
  constructor() {
    super();
    this.state = {
      // This is our Default number value
      NumberHolder: 0,
      GuessNumber: 0,
      chances: 3,
    };
  }

  componentDidMount() {
    const {chances, NumberHolder} = this.state;
    chances == 3 && this._new();
    // console.log(NumberHolder, chances);
  }

  _new = () => {
    this.setState({NumberHolder: Math.floor(Math.random() * 10) + 1});
  };

  _GenerateRandomNumber = () => {
    const {GuessNumber, chances, NumberHolder} = this.state;

    if (GuessNumber == NumberHolder) {
      ToastAndroid.show('True :)', ToastAndroid.SHORT);
      this.setState({chances: 3, GuessNumber: ''});
      this._new()
    } else {
      ToastAndroid.show('Wrong XD', ToastAndroid.SHORT);
      this.setState({chances: chances - 1, GuessNumber:''});
    }
  };

  render() {
    const {GuessNumber, NumberHolder, chances} = this.state;
    // console.log(NumberHolder);
    if(chances == 0){
        this.setState({chances: 3})
        this._new()
    }

    return (
      <View style={styles.MainContainer}>
        <View style={styles.textInput}>
          <CTextInput
            value={GuessNumber}
            // style={{fontSize: 20}}
            onChangeText={input => {
              this.setState({GuessNumber: input});
            }}
            placeholder="Masukkan Angka Tebakan"
          />
        </View>
        <Text style={{marginBottom: 10}}>
          Tebak Angka dari 1 sampai 10
        </Text>
        <Text>Kesempatan {chances}</Text>
        <CButton
          title="Tebak"
          onPress={this._GenerateRandomNumber}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    justifyContent: 'space-around',
    marginTop: 15,
    flexDirection: 'row',
  },
});
