const initialState = {
  test: 'Al Kocheng',
  students: [
    {id: 1, name: 'Mahrus', address: 'Tangerang'},
    {id: 2, name: 'Farid', address: 'Lampung'},
    {id: 3, name: 'Subhan', address: 'Jakarta'},
    {id: 4, name: 'Pur', address: 'Purwokerto'},
    {id: 5, name: 'Joni', address: 'Tangerang'},
    {id: 6, name: 'Dimitri', address: 'Rusia'},
  ],
  currentUser: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD-STUDENT':
      return {
        ...state, //Manggil statement
        students: [...state.students, action.payload], //Spread Operator, dimana dia tambahkan action.payload
      };
    case 'DELETE-STUDENT':
      return {
        ...state,
        students: state.students.filter(value => {
          return value.id != action.payload;
        }),
      };
    case 'LOGIN':
      return {
        ...state,
        currentUser: action.payload,
      };
    case 'UPDATE-DATA':
      return {
        ...state,
        students: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
